import 'package:driver_app/data/models/driver/driver_model.dart';
import 'package:driver_app/data/models/pendencies/pendencies_model.dart';

final mockDriver = DriverModel(
    id: 'id',
    firstName: 'firstName',
    lastName: 'lastName',
    displayName: 'displayName');

final mockPendencies = PendenciesModel(boxes: 0, orders: []);
