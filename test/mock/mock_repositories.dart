import 'package:driver_app/data/repositories/driver_repository/driver_repository_interface.dart';
import 'package:driver_app/data/repositories/pendencies_repository/pendencies_repository_interface.dart';
import 'package:mocktail/mocktail.dart';

class MockDriverRepository extends Mock implements DriverRepositoryInterface {}

class MockPendenciesRepository extends Mock
    implements PendenciesRepositoryInterface {}
