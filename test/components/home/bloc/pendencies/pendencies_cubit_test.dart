import 'package:driver_app/components/home/bloc/pendencies/pendencies_cubit.dart';
import 'package:driver_app/components/home/bloc/pendencies/pendencies_state.dart';
import 'package:driver_app/data/repositories/driver_repository/driver_repository_interface.dart';
import 'package:driver_app/data/repositories/pendencies_repository/pendencies_repository_interface.dart';
import 'package:driver_app/shared/strings.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../mock/mock_models.dart' as mock_models;
import '../../../../mock/mock_repositories.dart';

void main() {
  late DriverRepositoryInterface driverRepository;
  late PendenciesRepositoryInterface pendenciesRepository;
  late PendenciesCubit pendenciesCubit;

  setUp(() {
    driverRepository = MockDriverRepository();
    pendenciesRepository = MockPendenciesRepository();
    pendenciesCubit = PendenciesCubit(pendenciesRepository, driverRepository);
  });

  tearDown(() {
    pendenciesCubit.close();
  });

  test(
      'Should emit [PendenciesInitialState] when PendenciesCubit is initialized',
      () async {
    expect(pendenciesCubit.state, isA<PendenciesInitialState>());
  });

  group('PendenciesCubit [loadPendencies] method tests', () {
    test(
        'Should emit [PendenciesLoadingState, PendenciesLoadedState] when loadPendencies method is called',
        () {
      when(() => driverRepository.getUser())
          .thenAnswer((_) async => mock_models.mockDriver);
      when(() => pendenciesRepository
              .getDriverPendencies(any(that: isA<String>())))
          .thenAnswer((_) async => mock_models.mockPendencies);

      expectLater(
        pendenciesCubit.stream,
        emitsInOrder([
          isA<PendenciesLoadingState>(),
          isA<PendenciesLoadedState>()
              .having(
                (state) => state.pendencies.boxes,
                'boxes',
                0,
              )
              .having(
                (state) => state.pendencies.orders.length,
                'orders length',
                0,
              ),
        ]),
      );
      pendenciesCubit.loadPendencies();
    });

    test(
        'Should emit [PendenciesLoadingState, PendenciesErrorState] when loadingPendencies is called with null driver',
        () {
      when(() => driverRepository.getUser()).thenAnswer((_) async => null);

      expectLater(
          pendenciesCubit.stream,
          emitsInOrder([
            isA<PendenciesLoadingState>(),
            isA<PendenciesErrorState>().having((state) => state.message,
                'error message', Strings.driverLoadExceptionMessage),
          ]));

      pendenciesCubit.loadPendencies();
    });

    test(
        'Should emit [PendenciesLoadingState, PendenciesErrorState] when loadingPendencies is called with null pendencies',
        () {
      const exceptionMessage = 'Error';
      when(() => driverRepository.getUser())
          .thenAnswer((_) async => mock_models.mockDriver);
      when(() => pendenciesRepository.getDriverPendencies(
          any(that: isA<String>()))).thenThrow(exceptionMessage);

      expectLater(
          pendenciesCubit.stream,
          emitsInOrder([
            isA<PendenciesLoadingState>(),
            isA<PendenciesErrorState>().having(
                (state) => state.message, 'error message', exceptionMessage),
          ]));

      pendenciesCubit.loadPendencies();
    });
  });
}
