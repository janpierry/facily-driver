import 'package:driver_app/components/home/bloc/driver_info/driver_info_cubit.dart';
import 'package:driver_app/components/home/bloc/driver_info/driver_info_state.dart';
import 'package:driver_app/data/repositories/driver_repository/driver_repository_interface.dart';
import 'package:driver_app/shared/strings.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../mock/mock_models.dart' as mock_models show mockDriver;
import '../../../../mock/mock_repositories.dart';

void main() {
  late DriverRepositoryInterface driverRepository;
  late DriverInfoCubit driverCubit;

  setUp(() {
    driverRepository = MockDriverRepository();
    driverCubit = DriverInfoCubit(driverRepository);
  });

  tearDown(() {
    driverCubit.close();
  });

  test('Should emit [DriverInfoLoadingAllState] when cubit is initialized',
      () async {
    expect(driverCubit.state, isA<DriverInfoLoadingAllState>());
  });

  group('DriverInfoCubit [loadDriverInfo] method tests', () {
    test(
        'Should emit [DriverInfoLoadingState, DriverInfoLoadedState] when [loadDriverInfo] method is called',
        () async {
      when(() => driverRepository.getUser())
          .thenAnswer((invocation) async => mock_models.mockDriver);

      expectLater(
        driverCubit.stream,
        emitsInOrder(
          [
            isA<DriverInfoLoadingState>(),
            isA<DriverInfoLoadedState>().having(
              (state) => state.driver.hashCode,
              'hashCode',
              mock_models.mockDriver.hashCode,
            ),
          ],
        ),
      );
      driverCubit.loadDriverInfo();
    });

    test(
        'Should emit [DriverInfoLoadingState, DriverInfoErrorState] when [loadDriverInfo] method return null driver',
        () async {
      when(() => driverRepository.getUser())
          .thenAnswer((invocation) async => null);

      expectLater(
        driverCubit.stream,
        emitsInOrder(
          [
            isA<DriverInfoLoadingState>(),
            isA<DriverInfoErrorState>().having(
              (state) => state.message,
              'errorMessage',
              Strings.driverLoadExceptionMessage,
            ),
          ],
        ),
      );
      driverCubit.loadDriverInfo();
    });
  });
}
