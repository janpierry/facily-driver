class PendenciesModel {
  int boxes;
  List<int> orders;

  PendenciesModel({
    required this.boxes,
    required this.orders,
  });

  factory PendenciesModel.fromJason(Map<String, dynamic> json) {
    return PendenciesModel(
      boxes: json['boxes'],
      orders: List<int>.from(json['orders'].map((o) => o as int)),
    );
  }
}
