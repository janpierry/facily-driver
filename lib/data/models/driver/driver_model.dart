class DriverModel {
  String id;
  String firstName;
  String lastName;
  String displayName;

  DriverModel({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.displayName,
  });

  factory DriverModel.fromJson(Map<String, dynamic> json) {
    return DriverModel(
        id: json['data']['ID'],
        firstName: json['data']['first_name'],
        lastName: json['data']['last_name'],
        displayName: json['data']['display_name']);
  }
}
