class ShipmentModel {
  int id;
  int driverId;
  DateTime loadedAt;
  bool accepted;
  DateTime? acceptedAt;
  int? dispatchedBoxes;

  ShipmentModel({
    required this.id,
    required this.driverId,
    required this.loadedAt,
    required this.accepted,
    this.acceptedAt,
    this.dispatchedBoxes,
  });

  factory ShipmentModel.fromJson(Map<String, dynamic> json) {
    return ShipmentModel(
      id: json['id'],
      driverId: json['driver_id'],
      loadedAt: DateTime.parse(json['loaded_at']),
      accepted: json['accepted'],
      acceptedAt: json['accepted'] ? DateTime.parse(json['accepted_at']) : null,
      dispatchedBoxes: json['dispatched_boxes'],
    );
  }
}
