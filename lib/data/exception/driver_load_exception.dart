class DriverLoadException implements Exception {
  final String _message;

  DriverLoadException(this._message);

  @override
  String toString() {
    return _message;
  }
}
