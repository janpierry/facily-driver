import 'dart:convert';

import 'package:driver_app/data/services/http_service/custom_response.dart';
import 'package:http/http.dart';

class HttpService {
  final _client = Client();

  Future<CustomResponse> getRequest(String uri,
      {Map<String, dynamic>? params, Map<String, String>? headers}) async {
    if (params != null) {
      uri += _buildQueryString(params);
    }

    var httpResponse = await _client.get(Uri.parse(uri), headers: headers);
    return _parseHttpResponse(httpResponse);
  }

  CustomResponse _parseHttpResponse(Response httpResponse) {
    CustomResponse response;
    try {
      response = CustomResponse(
        statusCode: httpResponse.statusCode,
        content: jsonDecode(httpResponse.body),
      );
    } on Exception catch (e) {
      response = CustomResponse(statusCode: 500, exception: e);
    }

    return response;
  }

  String _buildQueryString(Map<String, dynamic> params) {
    var output = '?';
    params.forEach((key, value) {
      output += '$key=$value&';
    });

    return output;
  }
}
