class CustomResponse {
  int statusCode;
  Map<String, dynamic>? content;
  Exception? exception;

  CustomResponse({required this.statusCode, this.content, this.exception});

  bool get success => statusCode == 200;
}
