import 'package:driver_app/data/models/shipment/shipment_model.dart';

abstract class ShipmentRepositoryInterface {
  Future<List<ShipmentModel>> getDriverShipments(String driverId);
}
