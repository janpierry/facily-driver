import 'package:driver_app/data/models/shipment/shipment_model.dart';
import 'package:driver_app/data/repositories/shipment_repository/shipment_repository_interface.dart';
import 'package:driver_app/data/services/http_service/http_service.dart';
import 'package:driver_app/utils/strings_utils.dart';

class ShipmentRepository implements ShipmentRepositoryInterface {
  final HttpService _http;
  final _baseUrl = StringsUtils.driverApiUrl;

  ShipmentRepository(this._http);

  String _getToken() => StringsUtils.authToken;

  @override
  Future<List<ShipmentModel>> getDriverShipments(String driverId) async {
    final token = _getToken();
    final uri = '$_baseUrl/load-truck';
    final params = {'driver_id': driverId};
    final header = {'Authorization': 'Bearer $token'};
    final response =
        await _http.getRequest(uri, params: params, headers: header);

    final items = response.content!['items'];
    return List<ShipmentModel>.from(
        items.map((s) => ShipmentModel.fromJson(s)));
  }
}
