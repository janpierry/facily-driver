import 'package:driver_app/data/models/driver/driver_model.dart';
import 'package:driver_app/data/repositories/driver_repository/driver_repository_interface.dart';
import 'package:driver_app/data/services/http_service/http_service.dart';
import 'package:driver_app/utils/strings_utils.dart';

class DriverRepository implements DriverRepositoryInterface {
  final HttpService _http;
  final _baseUrl = StringsUtils.driverApiUrl;
  DriverModel? user;

  DriverRepository(this._http);

  String _getToken() => StringsUtils.authToken;

  @override
  Future<DriverModel?> getUser() async {
    if (user == null) {
      final token = _getToken();
      final uri = '$_baseUrl/users-me';
      final header = {'Authorization': 'Bearer $token'};
      final response = await _http.getRequest(uri, headers: header);
      user = DriverModel.fromJson(response.content!);
    }

    return user;
  }
}
