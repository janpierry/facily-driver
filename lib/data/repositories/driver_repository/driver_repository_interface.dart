import 'package:driver_app/data/models/driver/driver_model.dart';

abstract class DriverRepositoryInterface {
  Future<DriverModel?> getUser();
}
