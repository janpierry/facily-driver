import 'package:driver_app/data/models/pendencies/pendencies_model.dart';
import 'package:driver_app/data/repositories/pendencies_repository/pendencies_repository_interface.dart';
import 'package:driver_app/data/services/http_service/http_service.dart';
import 'package:driver_app/utils/strings_utils.dart';

class PendenciesRepository implements PendenciesRepositoryInterface {
  final HttpService _http;
  final _baseUrl = StringsUtils.driverApiUrl;

  PendenciesRepository(this._http);

  String _getToken() => StringsUtils.authToken;

  @override
  Future<PendenciesModel> getDriverPendencies(String driverId) async {
    final token = _getToken();
    final uri = '$_baseUrl/driver-pendency';
    final params = {'driver_id': driverId};
    final header = {'Authorization': 'Bearer $token'};
    final response =
        await _http.getRequest(uri, params: params, headers: header);

    return PendenciesModel.fromJason(response.content!);
  }
}
