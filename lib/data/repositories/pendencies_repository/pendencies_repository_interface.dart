import 'package:driver_app/data/models/pendencies/pendencies_model.dart';

abstract class PendenciesRepositoryInterface {
  Future<PendenciesModel> getDriverPendencies(String driverId);
}
