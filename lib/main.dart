import 'package:driver_app/components/home/ui/home_page.dart';
import 'package:driver_app/locator.dart';
import 'package:flutter/material.dart';

void main() {
  setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Facily Driver',
      debugShowCheckedModeBanner: false,
      initialRoute: HomePage.name,
      routes: {
        HomePage.name: (_) => const HomePage(),
      },
    );
  }
}
