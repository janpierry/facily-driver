import 'package:driver_app/data/repositories/driver_repository/driver_repository.dart';
import 'package:driver_app/data/repositories/driver_repository/driver_repository_interface.dart';
import 'package:driver_app/data/repositories/pendencies_repository/pendencies_repository.dart';
import 'package:driver_app/data/repositories/pendencies_repository/pendencies_repository_interface.dart';
import 'package:driver_app/data/repositories/shipment_repository/shipment_repository.dart';
import 'package:driver_app/data/repositories/shipment_repository/shipment_repository_interface.dart';
import 'package:driver_app/data/services/http_service/http_service.dart';
import 'package:get_it/get_it.dart';

var getIt = GetIt.instance;

void setupLocator() {
  getIt.registerLazySingleton(() => HttpService());
  getIt.registerLazySingleton<DriverRepositoryInterface>(
      () => DriverRepository(getIt.get<HttpService>()));
  getIt.registerLazySingleton<PendenciesRepositoryInterface>(
      () => PendenciesRepository(getIt.get<HttpService>()));
  getIt.registerLazySingleton<ShipmentRepositoryInterface>(
      () => ShipmentRepository(getIt.get<HttpService>()));
}
