import 'package:driver_app/data/models/pendencies/pendencies_model.dart';

abstract class PendenciesState {}

class PendenciesInitialState implements PendenciesState {}

class PendenciesLoadingState implements PendenciesState {}

class PendenciesLoadedState implements PendenciesState {
  final PendenciesModel pendencies;

  PendenciesLoadedState(this.pendencies);
}

class PendenciesErrorState implements PendenciesState {
  final String message;

  PendenciesErrorState(this.message);
}
