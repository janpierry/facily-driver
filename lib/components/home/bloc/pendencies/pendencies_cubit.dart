import 'package:driver_app/components/home/bloc/pendencies/pendencies_state.dart';
import 'package:driver_app/data/exception/driver_load_exception.dart';
import 'package:driver_app/data/repositories/driver_repository/driver_repository_interface.dart';
import 'package:driver_app/data/repositories/pendencies_repository/pendencies_repository_interface.dart';
import 'package:driver_app/shared/strings.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PendenciesCubit extends Cubit<PendenciesState> {
  final PendenciesRepositoryInterface _pendenciesRepository;
  final DriverRepositoryInterface _driverRepository;

  PendenciesCubit(this._pendenciesRepository, this._driverRepository)
      : super(PendenciesInitialState());

  Future<void> loadPendencies() async {
    emit(PendenciesLoadingState());
    try {
      final driver = await _driverRepository.getUser();
      if (driver != null) {
        final pendencies =
            await _pendenciesRepository.getDriverPendencies(driver.id);
        emit(PendenciesLoadedState(pendencies));
      } else {
        emit(PendenciesErrorState(Strings.driverLoadExceptionMessage));
      }
    } catch (e) {
      emit(PendenciesErrorState(e.toString()));
    }
  }
}
