import 'package:driver_app/data/models/shipment/shipment_model.dart';

abstract class ShipmentState {}

class ShipmentInitialState implements ShipmentState {}

class ShipmentLoadingState implements ShipmentState {}

class ShipmentLoadedState implements ShipmentState {
  final List<ShipmentModel> shipments;

  ShipmentLoadedState(this.shipments);
}

class ShipmentErrorState implements ShipmentState {
  final String message;

  ShipmentErrorState(this.message);
}
