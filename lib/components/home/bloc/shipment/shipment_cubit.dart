import 'package:driver_app/components/home/bloc/shipment/shipment_state.dart';
import 'package:driver_app/data/exception/driver_load_exception.dart';
import 'package:driver_app/data/repositories/driver_repository/driver_repository_interface.dart';
import 'package:driver_app/data/repositories/shipment_repository/shipment_repository_interface.dart';
import 'package:driver_app/shared/strings.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ShipmentCubit extends Cubit<ShipmentState> {
  final ShipmentRepositoryInterface _shipmentRepository;
  final DriverRepositoryInterface _driverRepository;

  ShipmentCubit(this._shipmentRepository, this._driverRepository)
      : super(ShipmentInitialState());

  Future<void> loadShipments() async {
    emit(ShipmentLoadingState());
    try {
      final driver = await _driverRepository.getUser();
      if (driver != null) {
        final shipments =
            await _shipmentRepository.getDriverShipments(driver.id);
        emit(ShipmentLoadedState(shipments));
      } else {
        throw DriverLoadException(Strings.driverLoadExceptionMessage);
      }
    } catch (e) {
      emit(ShipmentErrorState(e.toString()));
    }
  }
}
