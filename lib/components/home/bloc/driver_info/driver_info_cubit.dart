import 'package:driver_app/components/home/bloc/driver_info/driver_info_state.dart';
import 'package:driver_app/data/exception/driver_load_exception.dart';
import 'package:driver_app/data/repositories/driver_repository/driver_repository_interface.dart';
import 'package:driver_app/shared/strings.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DriverInfoCubit extends Cubit<DriverInfoState> {
  final DriverRepositoryInterface _driverRepository;

  DriverInfoCubit(this._driverRepository) : super(DriverInfoLoadingAllState());

  Future<void> loadDriverInfo() async {
    emit(DriverInfoLoadingState());
    final driver = await _driverRepository.getUser();
    if (driver != null) {
      emit(DriverInfoLoadedState(driver));
    } else {
      emit(DriverInfoErrorState(Strings.driverLoadExceptionMessage));
    }
  }
}
