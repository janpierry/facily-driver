import 'package:driver_app/data/models/driver/driver_model.dart';

abstract class DriverInfoState {}

class DriverInfoLoadingAllState implements DriverInfoState {}

class DriverInfoLoadingState implements DriverInfoState {}

class DriverInfoLoadedState implements DriverInfoState {
  final DriverModel driver;

  DriverInfoLoadedState(this.driver);
}

class DriverInfoErrorState implements DriverInfoState {
  final String message;

  DriverInfoErrorState(this.message);
}
