import 'package:driver_app/components/home/bloc/driver_info/driver_info_cubit.dart';
import 'package:driver_app/components/home/bloc/driver_info/driver_info_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class QRCodeWidget extends StatelessWidget {
  const QRCodeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(14),
        color: Colors.grey[200],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'N° de identificação:',
                    style: TextStyle(fontSize: 15),
                  ),
                  const SizedBox(height: 12),
                  BlocBuilder<DriverInfoCubit, DriverInfoState>(
                    builder: (context, state) {
                      if (state is DriverInfoLoadedState) {
                        return Text(
                          state.driver.id,
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 24),
                        );
                      }
                      return const SizedBox.shrink();
                    },
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  const Text(
                      'Utilize o QRCode sempre que for necessário confirmar sua identificação'),
                ],
              ),
            ),
            const SizedBox(width: 8),
            Expanded(child: BlocBuilder<DriverInfoCubit, DriverInfoState>(
              builder: (context, state) {
                if (state is DriverInfoLoadedState) {
                  return Image.network(
                      'https://southamerica-east1-facily-817c2.cloudfunctions.net/generate_qr_code?data=${state.driver.id}');
                }
                return const SizedBox.shrink();
              },
            )),
          ],
        ),
      ),
    );
  }
}
