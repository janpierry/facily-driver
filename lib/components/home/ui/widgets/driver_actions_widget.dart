import 'package:driver_app/components/home/bloc/shipment/shipment_cubit.dart';
import 'package:driver_app/components/home/bloc/shipment/shipment_state.dart';
import 'package:driver_app/shared/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DriverActionsWidget extends StatelessWidget {
  const DriverActionsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ShipmentCubit, ShipmentState>(
      builder: (context, state) {
        if (state is! ShipmentLoadedState) {
          return const SizedBox.shrink();
        }
        if (state.shipments.isEmpty) {
          return const SizedBox.shrink();
        }
        return Container(
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14),
            color: Colors.grey[200],
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'O que você deseja?',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                const SizedBox(height: 4),
                const Text('Selecione a opção abaixo'),
                const SizedBox(height: 16),
                Center(
                  child: Column(
                    children: const [
                      CustomButtom(
                        label: 'Verificar carregamentos',
                      ),
                      SizedBox(height: 20),
                      CustomButtom(
                        label: 'Entregar pedidos',
                      ),
                      SizedBox(height: 20),
                      CustomButtom(
                        label: 'Consultar NFe',
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
