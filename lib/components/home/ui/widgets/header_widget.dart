import 'package:driver_app/components/home/bloc/driver_info/driver_info_cubit.dart';
import 'package:driver_app/components/home/bloc/driver_info/driver_info_state.dart';
import 'package:driver_app/data/models/driver/driver_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HeaderWidget extends StatelessWidget {
  const HeaderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DriverInfoCubit, DriverInfoState>(
      buildWhen: (oldState, newState) {
        return newState is DriverInfoLoadingState ||
            newState is DriverInfoLoadedState;
      },
      builder: (context, state) {
        if (state is DriverInfoLoadingState) {
          return const Center(child: CircularProgressIndicator());
        } else if (state is DriverInfoLoadedState) {
          return _buildLoadedContent(state.driver);
        }
        return Container();
      },
    );
  }

  Widget _buildLoadedContent(DriverModel driverModel) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Olá, ${driverModel.firstName} ${driverModel.lastName}',
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          const SizedBox(height: 8),
          const Text(
            'Tudo bem?',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          ),
        ],
      ),
    );
  }
}
