import 'package:driver_app/components/home/bloc/pendencies/pendencies_cubit.dart';
import 'package:driver_app/components/home/bloc/pendencies/pendencies_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ShipmentReleasedCardWidget extends StatelessWidget {
  const ShipmentReleasedCardWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PendenciesCubit, PendenciesState>(
      builder: (context, state) {
        if (state is! PendenciesLoadedState) {
          return const SizedBox.shrink();
        }
        if (state.pendencies.boxes == 0 && state.pendencies.orders.isEmpty) {
          return const SizedBox.shrink();
        }
        return Container(
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14),
            color: Colors.green[200],
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text(
                  'Carregamento liberado',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                ),
                SizedBox(height: 8),
                Text(
                    'Dirija-se ao setor responsável e faça um novo carregamento'),
              ],
            ),
          ),
        );
      },
    );
  }
}
