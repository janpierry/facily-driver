import 'package:driver_app/components/home/bloc/driver_info/driver_info_cubit.dart';
import 'package:driver_app/components/home/bloc/driver_info/driver_info_state.dart';
import 'package:driver_app/components/home/bloc/pendencies/pendencies_cubit.dart';
import 'package:driver_app/components/home/bloc/shipment/shipment_cubit.dart';
import 'package:driver_app/components/home/ui/widgets/driver_actions_widget.dart';
import 'package:driver_app/components/home/ui/widgets/header_widget.dart';
import 'package:driver_app/components/home/ui/widgets/qr_code_widget.dart';
import 'package:driver_app/components/home/ui/widgets/shipment_released_card_widget.dart';
import 'package:driver_app/data/repositories/driver_repository/driver_repository_interface.dart';
import 'package:driver_app/data/repositories/pendencies_repository/pendencies_repository_interface.dart';
import 'package:driver_app/data/repositories/shipment_repository/shipment_repository_interface.dart';
import 'package:driver_app/locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  static const name = 'home-page';

  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _driverRepository = getIt.get<DriverRepositoryInterface>();
    final _pendenciesRepository = getIt.get<PendenciesRepositoryInterface>();
    final _shipmentRepository = getIt.get<ShipmentRepositoryInterface>();

    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              DriverInfoCubit(_driverRepository)..loadDriverInfo(),
        ),
        BlocProvider(
          create: (context) =>
              PendenciesCubit(_pendenciesRepository, _driverRepository)
                ..loadPendencies(),
        ),
        BlocProvider(
          create: (context) =>
              ShipmentCubit(_shipmentRepository, _driverRepository)
                ..loadShipments(),
        ),
      ],
      child: const HomeView(),
    );
  }
}

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Center(
            child: Text(
              'Facily Driver',
            ),
          ),
          leading: IconButton(
            onPressed: () {},
            icon: const Icon(Icons.menu),
          ),
          actions: [
            IconButton(
              onPressed: () => onRefreshClicked(context),
              icon: const Icon(
                Icons.refresh,
              ),
            )
          ],
        ),
        body: _buildBody(context));
  }

  Widget _buildBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        child: BlocBuilder<DriverInfoCubit, DriverInfoState>(
          builder: (context, state) {
            if (state is DriverInfoLoadingAllState) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            return Column(
              children: const [
                SizedBox(height: 20),
                HeaderWidget(),
                SizedBox(height: 20),
                ShipmentReleasedCardWidget(),
                SizedBox(height: 20),
                DriverActionsWidget(),
                SizedBox(height: 20),
                QRCodeWidget(),
                SizedBox(height: 20),
              ],
            );
          },
        ),
      ),
    );
  }

  void onRefreshClicked(BuildContext context) {
    context.read<DriverInfoCubit>().loadDriverInfo();
    context.read<PendenciesCubit>().loadPendencies();
    context.read<ShipmentCubit>().loadShipments();
  }
}
